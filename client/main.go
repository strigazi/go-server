package main

import (
        "fmt"
        "net/http"
        "os"
        "time"
)

func main() {
        var command = os.Args[1]
        // for do_request
        for true {
                //time.Sleep(50 * time.Millisecond)
                switch command {
                case "getRadnomEmployee":
                        getRadnomEmployee()
                case "getEmployees":
                        getEmployees()
                default:
                        fmt.Println("Use getRadnomEmployee or getEmployees")
                        os.Exit(1)
                }
        }
}

func getRadnomEmployee() {
        //monitoring how long it takes to respond
        start := time.Now()

        //do the request
        resp, err := http.Get("http://go-server-employees.cern.ch/employees/10001")
        if err != nil {
                panic(err)
        }
        defer resp.Body.Close()

        fmt.Println("http://go-server-employees.cern.ch/employees/", resp.StatusCode, "0.0.0.0", time.Now().Sub(start).Seconds())


}

func getEmployees() {
        //monitoring how long it takes to respond
        start := time.Now()

        //do the request
        resp, err := http.Get("http://go-server-employees.cern.ch/employees&limit=10")
        if err != nil {
                panic(err)
        }
        defer resp.Body.Close()

        fmt.Println("http://go-server-employees.cern.ch/employees&limit=10", resp.StatusCode, "0.0.0.0", time.Now().Sub(start).Seconds())


}
