package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type Config struct {
  Database database
}

type database struct {
  Server string
  Port string
  Database string
  User string
  Password string
}

type Employee struct {
	Emp_no string `json:"emp_no"`
	First_name string `json:"first_name"`
	Last_name string `json:"last_name"`
	Gender string `json:"gender"`
	Birth_date string `json:"birth_date"`
	Hire_date string `json:"hire_date"`
}

var db *sql.DB
var err error

func main() {
	//var config_file
	var configFile = os.Args[1]
	var conf Config
	if _, err := toml.DecodeFile(configFile, &conf); err != nil {
		panic(err.Error())
	}
	connString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", conf.Database.User, conf.Database.Password, conf.Database.Server, conf.Database.Port, conf.Database.Database)

	db, err = sql.Open("mysql", connString)
	if err != nil {
		panic(err.Error())
	}
	db.SetMaxOpenConns(800)
	db.SetMaxIdleConns(800)
	db.SetConnMaxLifetime(time.Minute * 15)
	defer db.Close()
	// Prometheus: Histogram to collect required metrics
	histogram := prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "greeting_seconds",
		Help:    "Time take to greet someone",
		//Buckets: []float64{0.001, 0.002, 0.004, 0.008, 0.016, 0.064, 0.128, 0.256, 0.512, 1.024, 2.048}, //defining small buckets as this app should not take more than 1 sec to respond
	}, []string{"type"}) // this will be partitioned by the HTTP code.

	router := mux.NewRouter()
	//router.Handle("/sayhello/{name}", Sayhello(histogram))
	router.HandleFunc("/employees", getEmployees(histogram)).Methods("GET")
	router.HandleFunc("/employees/{emp_no}", getEmployee(histogram)).Methods("GET")
	router.Handle("/metrics", promhttp.Handler()) //Metrics endpoint for scrapping

	//Registering the defined metric with Prometheus
	prometheus.Register(histogram)

	log.Fatal(http.ListenAndServe(":14000", router))
}

func getEmployee(histogram *prometheus.HistogramVec) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		start := time.Now()
		code := 500
		defer func() {
			httpDuration := time.Now().Sub(start)
			histogram.WithLabelValues(fmt.Sprintf("%d", code)).Observe(httpDuration.Seconds())
		}()

		params := mux.Vars(r)
		result, err := db.Query("SELECT emp_no, first_name, last_name, gender, birth_date, hire_date FROM employees WHERE emp_no = ?", params["emp_no"])
		if err != nil {
			panic(err.Error())
		}
		defer result.Close()
		var employee Employee
		for result.Next() {
			err := result.Scan(&employee.Emp_no,
				&employee.First_name,
				&employee.Last_name,
				&employee.Gender,
				&employee.Birth_date,
				&employee.Hire_date)
			if err != nil {
				panic(err.Error())
			}
		}
		code = http.StatusBadRequest // if req is not GET
		if r.Method == "GET" {
			code = http.StatusOK
			err = json.NewEncoder(w).Encode(employee)
			if err != nil {
				panic(err.Error())
			}
		} else {
			w.WriteHeader(code)
		}

	}
}

func getEmployees(histogram *prometheus.HistogramVec) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		keys, ok := r.URL.Query()["limit"]
		if !ok {
			fmt.Println("limit is missing")
		}
		query := "SELECT emp_no, first_name, last_name, gender, birth_date, hire_date FROM employees"
		if len(keys[0]) == 1 {
			query = query + " LIMIT " + keys[0]
		}
		start := time.Now()
		code := 500
		defer func() {
			httpDuration := time.Now().Sub(start)
			histogram.WithLabelValues(fmt.Sprintf("%d", code)).Observe(httpDuration.Seconds())
		}()

		result, err := db.Query(query)
		if err != nil {
			panic(err.Error())
		}
		defer result.Close()
		var employees []Employee
		for result.Next() {
			var employee Employee
			err := result.Scan(&employee.Emp_no,
				&employee.First_name,
				&employee.Last_name,
				&employee.Gender,
				&employee.Birth_date,
				&employee.Hire_date)
			if err != nil {
				panic(err.Error())
			}

			employees = append(employees, employee)
		}
		code = http.StatusBadRequest // if req is not GET
		if r.Method == "GET" {
			code = http.StatusOK
			err = json.NewEncoder(w).Encode(employees)
			if err != nil {
				panic(err.Error())
			}
		} else {
			w.WriteHeader(code)
		}

	}
}